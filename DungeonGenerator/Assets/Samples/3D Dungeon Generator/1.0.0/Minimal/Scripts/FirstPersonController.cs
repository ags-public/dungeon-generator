using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonController : MonoBehaviour
{
    public float speed = 5.0f;  // movement speed
    public float mouseSensitivity = 100.0f;  // mouse sensitivity
    public float jumpForce = 5.0f;  // jump force
    public LayerMask groundMask;  // layers considered to be ground

    private CharacterController controller;
    private float verticalVelocity = 0.0f;
    private bool isGrounded = false;

    public Transform lookTransform;

    // Start is called before the first frame update
    void Start()
    {
        // get character controller and camera transform
        controller = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        // get input for movement and rotation
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        // move character based on input
        Vector3 movement = lookTransform.forward * verticalInput + lookTransform.right * horizontalInput;
        movement = Vector3.ClampMagnitude(movement, 1.0f);
        movement *= speed * Time.deltaTime;

        // apply gravity and jump force
        if (isGrounded && Input.GetKeyDown(KeyCode.Space))
        {
            verticalVelocity = jumpForce;
        }
        else if (!isGrounded)
        {
            verticalVelocity += Physics.gravity.y * Time.deltaTime;
        }

        // check for ground
        isGrounded = Physics.CheckSphere(transform.position, 0.2f, groundMask);

        // move character and apply gravity
        movement += Vector3.up * verticalVelocity * Time.deltaTime;
        controller.Move(movement);
    }
}