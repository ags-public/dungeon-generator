using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Generation
{
    [CustomEditor(typeof(DungeonGeneratorMinimal))]
    public class DungeonGeneratorEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            var generator = target as DungeonGeneratorBase;

            if (GUILayout.Button("Create Dungeon"))
            {
                generator.DestroyDungeon();
                generator.CreateDungeon();
            }

            if (GUILayout.Button("Create Dungeon (new seed)"))
            {
                int newSeed = Random.Range(0, int.MaxValue -1);
                generator.DestroyDungeon();
                generator.CreateDungeon(newSeed);
            }

        }
    }
}


