# About 3D Dungeon Generator

Developed by Amazing Games Studio, this package allows you to generate 3D-grid-based dungeons with a click of button.

# Installation

No UPM support at the moment. Please copy the Packages/com.amazinggs.dungeon-generator folder into your project.

# Samples

Import the Minimal sample to get started.

# Technical details
## Requirements

* 2022.2 and later (recommended)

## Package contents

The following table indicates the folder structure of the package:

|Location|Description|
|---|---|
|`<Editor>`|Root folder containing the source for the DungeonGenerator Editor.|
|`<Runtime>`|Root folder containing the source for the DungeonGenerator classes and its dependencies.|

## Document revision history

|Date|Reason|
|---|---|
|April 12th, 2022|Document created. Matches package version 1.0.0|
