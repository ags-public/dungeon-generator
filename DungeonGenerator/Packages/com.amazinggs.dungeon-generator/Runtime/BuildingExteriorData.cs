using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Generation.Interior;

namespace Generation
{
    [System.Flags]
    public enum WallOrientation
    { 
        None,
        PositiveX = 1,
        PositiveZ = 2,
        NegativeX = 4,
        NegativeZ = 8
    }

    [System.Serializable]
    public class BuildingExteriorData
    {
        [SerializeField] int m_buildingID;
        [SerializeField] float m_BuildingTileScale;
        [SerializeField] BoundsInt m_Bounds;

        Dictionary<Vector3Int, WallOrientation> m_Doors;

        public BuildingExteriorData()
        {
            m_Doors = new();
            m_BuildingTileScale = 1f;
        }

        public BuildingExteriorData(int id, Vector3Int location, Vector3Int size, float tileScale)
        {
            m_buildingID = id;
            m_Doors = new();
            m_Bounds = new BoundsInt(location, size);
            m_BuildingTileScale = tileScale;
        }

        public void AddDoor(Vector3Int hallwayTile,  WallOrientation newDoor)
        {
            if(!m_Doors.ContainsKey(hallwayTile))
                m_Doors[hallwayTile] = newDoor;
            else
                m_Doors[hallwayTile] |= newDoor;
        }

        public bool HasDoor(Vector3Int hallwayTile, WallOrientation checkDoor)
        {
            if (m_Doors.ContainsKey(hallwayTile))
                return (m_Doors[hallwayTile] & checkDoor) != 0;

            return false;
        }

        public DoorRoomData GetDoorRoomData(Vector3Int tile)
        {
            if (m_Doors.ContainsKey(tile))
            {
                return new DoorRoomData 
                { 
                    roomCoordinate = tile, 
                    doorOrientations = m_Doors[tile] 
                };
            }

            return null;
        }

        public Vector3Int FindClosestTile(Vector3Int checkTile)
        {
            //TODO: Optimize using edges or corners
            Vector3Int closestTile = -Vector3Int.one;
            float shortestDistance = float.MaxValue;

            if(checkTile.y >= Bounds.yMin && checkTile.y <= Bounds.yMax)
            {
                for (int x = Bounds.xMin; x < Bounds.xMax; x++)
                {
                    for (int z = Bounds.zMin; z < Bounds.zMax; z++)
                    {
                        var tile = new Vector3Int(x, checkTile.y, z);
                        float distance = (tile - checkTile).sqrMagnitude;
                        if (distance < shortestDistance)
                        {
                            shortestDistance = distance;
                            closestTile = tile;
                        }
                    }
                }
            }
            else
            {
                Debug.LogError($"{checkTile} is outside of Y bounds");
            }

            return closestTile;
        }

        public bool Intersect(BoundsInt b)
        {
            return 
                 !((this.Bounds.position.x >= (b.position.x + b.size.x)) || ((this.Bounds.position.x + this.Bounds.size.x) <= b.position.x)
                || (this.Bounds.position.y >= (b.position.y + b.size.y)) || ((this.Bounds.position.y + this.Bounds.size.y) <= b.position.y)
                || (this.Bounds.position.z >= (b.position.z + b.size.z)) || ((this.Bounds.position.z + this.Bounds.size.z) <= b.position.z));
        }

        public int ID { get { return m_buildingID; } }
        public BoundsInt Bounds { get { return m_Bounds; } }
        public float TileScale { get { return m_BuildingTileScale; } }
    }
}

