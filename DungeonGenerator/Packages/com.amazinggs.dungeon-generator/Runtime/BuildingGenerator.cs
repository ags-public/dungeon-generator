using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Generation.Interior;

namespace Generation
{
    public class BuildingGenerator : MonoBehaviour
    {
        readonly static Color k_DebugDimensionColor = new Color(0, 0, 1, 0.5f);

        public bool SpawnOnStart;
        public bool addDebugDoors;

        [SerializeField] BuildingExteriorData m_ExteriorData;

        public DoorRoomData[] doors;

        [Header("Prefabs")]
        public GameObject wallDoorPrefab;
        public GameObject wallWindowPrefab;
        public GameObject wallColumnPrefab;

        void Start()
        {
#if UNITY_EDITOR
            if (SpawnOnStart)
            {
                GenerateBuilding();
            }
#endif
        }

        public void Init(BuildingExteriorData data)
        {
            m_ExteriorData = data;

            GenerateBuilding();
        }

        void GenerateBuilding()
        {
            transform.localScale = m_ExteriorData.Bounds.size.ToVector3() * m_ExteriorData.TileScale;

            if(addDebugDoors)
                foreach (var door in doors)
                {
                    m_ExteriorData.AddDoor(door.roomCoordinate, door.doorOrientations);
                }

            SpawnExteriorWalls();

            var interiorGenerator = GetComponent<BuildingInteriorGenerator>();
            interiorGenerator.GenerateInterior(m_ExteriorData);
        }

        void SpawnExteriorWalls()
        {
            var extBounds = m_ExteriorData.Bounds;

            int minX = extBounds.xMin;
            int maxX = extBounds.xMax-1;

            int minZ = extBounds.zMin;
            int maxZ = extBounds.zMax-1;

            for (int y = extBounds.yMin; y < extBounds.yMax; y++)
            {
                //corners
                var minXminZ = new Vector3Int(minX, y, minZ);
                SpawnBuildingEdge(minXminZ, WallOrientation.NegativeX);
                SpawnBuildingEdge(minXminZ, WallOrientation.NegativeZ);

                var maxXmaxZ = new Vector3Int(maxX, y, maxZ);
                SpawnBuildingEdge(maxXmaxZ, WallOrientation.PositiveX);
                SpawnBuildingEdge(maxXmaxZ, WallOrientation.PositiveZ);

                var maxXminZ = new Vector3Int(maxX, y, minZ);
                if (extBounds.size.x > 1)
                {
                    SpawnBuildingEdge(maxXminZ, WallOrientation.NegativeZ);
                }
                SpawnBuildingEdge(maxXminZ, WallOrientation.PositiveX);

                var minXmaxZ = new Vector3Int(minX, y, maxZ);
                if (extBounds.size.z > 1)
                {
                    SpawnBuildingEdge(minXmaxZ, WallOrientation.NegativeX);
                }
                SpawnBuildingEdge(minXmaxZ, WallOrientation.PositiveZ);

                //sides
                for (int x = minX + 1; x < maxX; x++)
                {
                    SpawnBuildingEdge(new Vector3Int(x, y, minZ), WallOrientation.NegativeZ);
                    SpawnBuildingEdge(new Vector3Int(x, y, maxZ), WallOrientation.PositiveZ);
                }

                for (int z = minZ + 1; z < maxZ; z++)
                {
                    SpawnBuildingEdge(new Vector3Int(minX, y, z), WallOrientation.NegativeX);
                    SpawnBuildingEdge(new Vector3Int(maxX, y, z), WallOrientation.PositiveX);
                }
            }
        }

        void SpawnBuildingEdge(Vector3Int tileCoordinate, WallOrientation orientation)
        {
            if (m_ExteriorData.HasDoor(tileCoordinate, orientation))
                SpawnDoor(tileCoordinate, orientation);
            else
                SpawnExteriorWall(tileCoordinate, orientation);

            SpawnColumn(tileCoordinate, orientation);
        }

        void SpawnExteriorWall(Vector3Int tileCoordinate, WallOrientation orientation)
        {
            var offset = GetExteriorWallOffset(orientation);
            Vector3 roomPosition = (tileCoordinate.ToVector3() + offset.Item1) * m_ExteriorData.TileScale;

            var go = Instantiate(wallWindowPrefab, roomPosition, offset.Item2);
            go.transform.localScale = Vector3.one * m_ExteriorData.TileScale;
            go.name = $"Wall [{tileCoordinate}]: {orientation}";

            go.transform.SetParent(transform,true);
        }

        void SpawnDoor(Vector3Int doorLocation, WallOrientation doorOrientation)
        {
            var offset = GetExteriorWallOffset(doorOrientation);

            Vector3 roomPosition = (doorLocation.ToVector3() + offset.Item1) * m_ExteriorData.TileScale;
            var go = Instantiate(wallDoorPrefab, roomPosition, offset.Item2);
            go.transform.localScale = Vector3.one * m_ExteriorData.TileScale;
            go.name = $"Door [{doorLocation}]: {doorOrientation}";

            go.transform.SetParent(transform, true);
        }

        void SpawnColumn(Vector3Int tileCoordinate, WallOrientation orientation)
        {
            Vector3 columnOffset = Vector3.zero;
            switch (orientation)
            {
                case WallOrientation.PositiveX:
                    columnOffset = new Vector3(1, 0, 0);
                    break;
                case WallOrientation.PositiveZ:
                    columnOffset = new Vector3(1, 0, 1);
                    break;
                case WallOrientation.NegativeX:
                    columnOffset = new Vector3(0, 0, 1);
                    break;
                case WallOrientation.NegativeZ:
                    columnOffset = new Vector3(0, 0, 0);
                    break;
            }

            Vector3 position = (tileCoordinate.ToVector3() + columnOffset) * m_ExteriorData.TileScale;
            var go = Instantiate(wallColumnPrefab, position, Quaternion.identity);

            go.transform.localScale = Vector3.one * m_ExteriorData.TileScale;
            go.transform.SetParent(transform, true);
        }

        (Vector3, Quaternion) GetExteriorWallOffset(WallOrientation orientation)
        {
            Vector3 position = Vector3.zero;
            Quaternion rotation = Quaternion.identity;

            switch (orientation)
            {
                case WallOrientation.PositiveX:
                    position = new Vector3(1f-0.1f, 0, 1);
                    rotation = Quaternion.Euler(0, 90, 0);
                    break;

                case WallOrientation.NegativeX:
                    position = new Vector3(0f, 0, 1f);
                    rotation = Quaternion.Euler(0, 90, 0);
                    break;

                case WallOrientation.PositiveZ:
                    position = new Vector3(0, 0, 1f-0.1f);
                    break;

                case WallOrientation.NegativeZ:
                    position = new Vector3(0f, 0, 0f);
                    break;
            }

            return (position, rotation);
        }
    }
}