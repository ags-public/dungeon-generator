using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Generation.Interior
{

    [System.Serializable]
    public class BuildingInteriorData 
    {
        [SerializeField] BoundsInt m_Bounds;
        [SerializeField] List<FloorData> m_FloorsData;
        [SerializeField] List<StaircaseData> m_StaircasesData;
        [SerializeField] List<LadderData> m_LaddersData;

        public BuildingInteriorData(BuildingExteriorData exteriorData)
        {
            m_Bounds = exteriorData.Bounds;
            m_FloorsData = new();
            m_StaircasesData = new();
            m_LaddersData = new(); 

            for (int iFloor = 0; iFloor < FloorCount; iFloor++)
            {
                var floor = new FloorData(iFloor, exteriorData);
                m_FloorsData.Add(floor);
            }

            ConnectAllFloors();
        }

        void ConnectAllFloors()
        {
            if (FloorCount == 1) return;

            for (int iFloor = 1; iFloor < FloorCount; iFloor++)
            {
                ConnectFloors(m_FloorsData[iFloor - 1], m_FloorsData[iFloor]);
            }
        }

        void ConnectFloors(FloorData bottomFloor, FloorData topFloor)
        {
            //check corner
            if (m_Bounds.size.x == 1 && m_Bounds.size.z == 1)
            {
                var doors = bottomFloor.GetRoom(0, 0).Doors;
                AddLadder(0, bottomFloor.FloorHeight, 0, doors);
                return;
            }

            int stairX, stairZ;
            bool useStaircase = ConnectFloorsWithStaircase(bottomFloor, topFloor, out stairX, out stairZ);
            if(useStaircase)
            {
                var topDoor = topFloor.GetDoorRooms()[0].roomCoordinate;
                var stairOrientation = CalculateStaircaseOrientation(stairX, stairZ, topDoor.x, topDoor.z);
                AddStaircase(stairX, stairZ, bottomFloor.FloorHeight, topFloor.FloorHeight, stairOrientation);
            }

        }

        WallOrientation CalculateStaircaseOrientation(int stairX, int stairZ, int doorX, int doorZ)
        {
            int directionX = doorX - stairX;
            int directionZ = doorZ - stairZ;

            //if(directionX == 0)
            {
                if (directionZ > 0)
                    return WallOrientation.PositiveZ;

                if (directionZ < 0)
                    return WallOrientation.NegativeZ;
            }

            //if (directionZ == 0)
            {
                if (directionX > 0)
                    return WallOrientation.PositiveX;

                if (directionX < 0)
                    return WallOrientation.NegativeX;
            }

            return WallOrientation.None;
        }

        bool ConnectFloorsWithStaircase(FloorData bottomFloor, FloorData topFloor, out int x, out int z)
        {
            if (m_Bounds.size.x == 1)
            {
                x = 0;
                z = 0;
                for (; z < m_Bounds.size.z; z++)
                {
                    if (FloorData.DoesNeitherRoomHaveDoors(0, z, bottomFloor, topFloor))
                        return true;
                }
            }


            if (m_Bounds.size.z == 1)
            {
                x = 0;
                z = 0;
                for (; x < m_Bounds.size.x; x++)
                {
                    if (FloorData.DoesNeitherRoomHaveDoors(x,0, bottomFloor, topFloor))
                        return true;
                }
            }


            int maxX = m_Bounds.size.x -1;
            int maxZ = m_Bounds.size.z -1;

            //check corners
            x = 0;
            z = 0;
            if (FloorData.DoesNeitherRoomHaveDoors(x, z, bottomFloor, topFloor))
                return true;

            x = 0;
            z = maxZ;
            if (FloorData.DoesNeitherRoomHaveDoors(x, z, bottomFloor, topFloor))
                return true;

            x = maxX;
            z = 0;
            if (FloorData.DoesNeitherRoomHaveDoors(x, z, bottomFloor, topFloor))
                return true;

            x = maxX;
            z = maxZ;
            if (FloorData.DoesNeitherRoomHaveDoors(x, z, bottomFloor, topFloor))
                return true;

            //check all
            for (x = 0; x < m_Bounds.size.x; x++)
            {
                for (z = 0; z < m_Bounds.size.z; z++)
                {
                    if (FloorData.DoesNeitherRoomHaveDoors(x, z, bottomFloor, topFloor))
                        return true;
                }
            }

            return false;
        }

        void AddStaircase(int x, int z, int yBot, int yTop, WallOrientation stairOrientation)
        {
            var staircase = new StaircaseData {
                x = x,
                z = z,
                yBot = yBot,
                yTop = yTop,
                stairOrientation = stairOrientation
            };
            m_StaircasesData.Add(staircase);
        }

        void AddLadder(int x, int y, int z, DoorRoomData doors)
        {
            for (int i = 0; i < 4; i++)
            {
                WallOrientation orientation = (WallOrientation)(1 << i);
                if (!doors.HasDoor(orientation))
                {
                    var ladderData = new LadderData
                    {
                        x = x,
                        y = y,
                        z = z,
                        ladderOrientation = orientation
                    };
                    m_LaddersData.Add(ladderData);

                    break;
                }
            }

        }

        public void ShowDebugBuilding(float tileScale)
        {
            foreach (var floor in m_FloorsData)
            {
                floor.ShowDebugFloor(tileScale);
            }
        }

        int FloorCount => m_Bounds.size.y;

        public StaircaseData[] Staircases => m_StaircasesData.ToArray();
        public LadderData[] Ladders => m_LaddersData.ToArray();
        public FloorData[] Floors => m_FloorsData.ToArray();


        public BoundsInt Bounds { get { return m_Bounds; } }
    }

    public class StaircaseData
    {
        public int x, z;
        public int yBot, yTop;
        public WallOrientation stairOrientation;
    }
    public class LadderData
    {
        public int x, y, z;
        public WallOrientation ladderOrientation;
    }
}