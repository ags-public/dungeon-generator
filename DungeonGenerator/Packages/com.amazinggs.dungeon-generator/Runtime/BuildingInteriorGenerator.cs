using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Generation.Interior;

namespace Generation
{
    public class BuildingInteriorGenerator : MonoBehaviour
    {
        public GameObject staircasePrefab;
        public GameObject ladderPrefab;
        public GameObject floorPrefab;

        [SerializeField] BuildingInteriorData m_InteriorData;
        float m_TileScale;

        public void GenerateInterior(BuildingExteriorData exteriorData)
        {
            m_InteriorData = new BuildingInteriorData(exteriorData);
            m_TileScale = exteriorData.TileScale;

            var staircases = m_InteriorData.Staircases;
            for (int i = 0; i < staircases.Length; i++)
            {
                SpawnStaircase(staircases[i]);
            }

            var ladders = m_InteriorData.Ladders;
            for (int i = 0; i < ladders.Length; i++)
            {
                SpawnLadder(ladders[i]);
            }

            var floors = m_InteriorData.Floors;
            for (int y = 1; y < floors.Length; y++)
            {
                SpawnFloor(floors[y], m_TileScale);
            }
        }

        void SpawnFloor(FloorData floorData, float tileScale)
        {
            var rooms = floorData.Rooms;
            for (int i = 0; i < rooms.Length; i++)
            {
                var room = rooms[i];

                var position = room.roomPosition.ToVector3() * tileScale;
                var go = Instantiate(floorPrefab, position, Quaternion.identity);
                go.transform.localScale = Vector3.one * tileScale;
            }
        }

        void SpawnStaircase(StaircaseData staircaseData)
        {
            var position = new Vector3(staircaseData.x, staircaseData.yBot, staircaseData.z);
            var go = Instantiate(staircasePrefab, position, Quaternion.identity);
        }

        void SpawnLadder(LadderData ladderData)
        {
            var position = new Vector3(ladderData.x, ladderData.y, ladderData.z);
            var go = Instantiate(staircasePrefab, position, Quaternion.identity);
        }

        void OnDrawGizmosSelected()
        {
            m_InteriorData.ShowDebugBuilding(m_TileScale);
        }
    }

}