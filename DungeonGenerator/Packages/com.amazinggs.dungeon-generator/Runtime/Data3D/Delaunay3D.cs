﻿/* Adapted from https://github.com/Bl4ckb0ne/delaunay-triangulation

Copyright (c) 2015-2019 Simon Zeni (simonzeni@gmail.com)


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:


The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.


THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.*/

using System.Collections.Generic;
using UnityEngine;
using Graphs;

namespace Delaunay
{

    [System.Serializable]
    public class Delaunay3D
    {

        public static bool AlmostEqual(Vertex left, Vertex right)
        {
            return (left.Position - right.Position).sqrMagnitude < 0.01f;
        }

        [SerializeField] List<Vertex> vertices;
        [SerializeField] List<Edge> edges;
        [SerializeField] List<Triangle> triangles;
        [SerializeField] List<Tetrahedron> tetrahedra;

        [SerializeField] Tetrahedron SuperTetrahedron;
        //public Triangle SuperTriangle;

        Delaunay3D()
        {
            edges = new List<Edge>();
            triangles = new List<Triangle>();
            tetrahedra = new List<Tetrahedron>();
        }

        public static Delaunay3D Triangulate(List<Vertex> vertices)
        {
            Delaunay3D delaunay = new Delaunay3D();
            delaunay.vertices = new List<Vertex>(vertices);
            delaunay.TriangulateV4();

            return delaunay;
        }


#if true
        void TriangulateV1()
        {
            float minX = Vertices[0].Position.x;
            float minY = Vertices[0].Position.y;
            float minZ = Vertices[0].Position.z;
            float maxX = minX;
            float maxY = minY;
            float maxZ = minZ;

            foreach (var vertex in Vertices)
            {
                if (vertex.Position.x < minX) minX = vertex.Position.x;
                if (vertex.Position.x > maxX) maxX = vertex.Position.x;
                if (vertex.Position.y < minY) minY = vertex.Position.y;
                if (vertex.Position.y > maxY) maxY = vertex.Position.y;
                if (vertex.Position.z < minZ) minZ = vertex.Position.z;
                if (vertex.Position.z > maxZ) maxZ = vertex.Position.z;
            }

            float dx = maxX - minX;
            float dy = maxY - minY;
            float dz = maxZ - minZ;
            float deltaMax = Mathf.Max(dx, dy, dz) * 1.41421356f;

            Vertex p1 = new Vertex(new Vector3(minX - 1, minY - 1, minZ - 1));
            Vertex p2 = new Vertex(new Vector3(maxX + deltaMax, minY - 1, minZ - 1));
            Vertex p3 = new Vertex(new Vector3(minX - 1, maxY + deltaMax, minZ - 1));
            Vertex p4 = new Vertex(new Vector3(minX - 1, minY - 1, maxZ + deltaMax));

            SuperTetrahedron = new Tetrahedron(p1, p2, p3, p4);
            Tetrahedra.Add(SuperTetrahedron);

            //Generate tetrahedros
            foreach (var vertex in Vertices)
            {
                List<Triangle> triangles = new List<Triangle>();

                foreach (var t in Tetrahedra)
                {
                    if (t.CircumCircleContains(vertex.Position))
                    {
                        t.IsBad = true;
                        triangles.Add(new Triangle(t.A, t.B, t.C));
                        triangles.Add(new Triangle(t.A, t.B, t.D));
                        triangles.Add(new Triangle(t.A, t.C, t.D));
                        triangles.Add(new Triangle(t.B, t.C, t.D));
                    }
                }

                for (int i = 0; i < triangles.Count; i++)
                {
                    for (int j = i + 1; j < triangles.Count; j++)
                    {
                        if (Triangle.AlmostEqual(triangles[i], triangles[j]))
                        {
                            triangles[i].IsBad = true;
                            triangles[j].IsBad = true;
                        }
                    }
                }

                Tetrahedra.RemoveAll((Tetrahedron t) => t.IsBad);
                triangles.RemoveAll((Triangle t) => t.IsBad);

                foreach (var triangle in triangles)
                {
                    Tetrahedra.Add(new Tetrahedron(triangle.a, triangle.b, triangle.c, vertex));
                }
            }

            //Tetrahedra.Remove(SuperTetrahedron);
            Tetrahedra.RemoveAll((Tetrahedron t) => t.ContainsVertex(p1) || t.ContainsVertex(p2) || t.ContainsVertex(p3) || t.ContainsVertex(p4));

            HashSet<Triangle> triangleSet = new HashSet<Triangle>();
            HashSet<Edge> edgeSet = new HashSet<Edge>();

            foreach (var t in Tetrahedra)
            {

                var abc = new Triangle(t.A, t.B, t.C);
                var abd = new Triangle(t.A, t.B, t.D);
                var acd = new Triangle(t.A, t.C, t.D);
                var bcd = new Triangle(t.B, t.C, t.D);

                Triangle[] triangles = new Triangle[] { abc, abd, acd, bcd };
                for (int iTris = 0; iTris < triangles.Length; iTris++)
                {
                    if (triangleSet.Add(triangles[iTris]))
                        Triangles.Add(abc);
                }

                var ab = new Edge(t.A, t.B);
                var bc = new Edge(t.B, t.C);
                var ca = new Edge(t.C, t.A);
                var da = new Edge(t.D, t.A);
                var db = new Edge(t.D, t.B);
                var dc = new Edge(t.D, t.C);

                Edge[] edges = new Edge[] { ab, bc, ca, da, db, dc };
                for (int iEdge = 0; iEdge < edges.Length; iEdge++)
                {
                    if (edgeSet.Add(edges[iEdge]))
                        Edges.Add(edges[iEdge]);
                }

            }

        }
        void TriangulateV2()
        {
            float minX = Vertices[0].Position.x;
            float minY = Vertices[0].Position.y;
            float minZ = Vertices[0].Position.z;
            float maxX = minX;
            float maxY = minY;
            float maxZ = minZ;

            foreach (var vertex in Vertices)
            {
                if (vertex.Position.x < minX) minX = vertex.Position.x;
                if (vertex.Position.x > maxX) maxX = vertex.Position.x;
                if (vertex.Position.y < minY) minY = vertex.Position.y;
                if (vertex.Position.y > maxY) maxY = vertex.Position.y;
                if (vertex.Position.z < minZ) minZ = vertex.Position.z;
                if (vertex.Position.z > maxZ) maxZ = vertex.Position.z;
            }

            float dx = maxX - minX;
            float dy = maxY - minY;
            float dz = maxZ - minZ;
            float deltaMax = Mathf.Max(dx, dy, dz) * 1.41421356f;

            Vertex p1 = new Vertex(new Vector3(minX - 1, minY - 1, minZ - 1));
            Vertex p2 = new Vertex(new Vector3(maxX + deltaMax, minY - 1, minZ - 1));
            Vertex p3 = new Vertex(new Vector3(minX - 1, maxY + deltaMax, minZ - 1));
            Vertex p4 = new Vertex(new Vector3(minX - 1, minY - 1, maxZ + deltaMax));

            SuperTetrahedron = new Tetrahedron(p1, p2, p3, p4);
            //Tetrahedra.Add(SuperTetrahedron);

            var triangulation = new List<Triangle>();
            triangulation.AddRange(SuperTetrahedron.GenerateTriangles());

            //Generate tetrahedros
            foreach (var vertex in Vertices)
            {
                HashSet<Triangle> badTriangles = new HashSet<Triangle>();

                // for each triangle in triangulation do // !first find all the triangles that are no longer valid due to the insertion
                foreach (var t in triangulation)
                {
                    //if point is inside circumcircle of triangle
                    if (t.CircumCircleContains(vertex.Position))
                    {
                        //t.IsBad = true;
                        badTriangles.Add(t);
                    }
                }

                //polygon := empty set
                List<Edge> polygon = new List<Edge>();

                //for each triangle in badTriangles do // find the boundary of the polygonal hole
                foreach (var badT in badTriangles)
                {
                    //for each edge in triangle do
                    Edge[] badEdges = badT.GenerateEdges();
                    foreach (var edge in badEdges)
                    {
                        //  if edge is not shared by any other triangles in badTriangles
                        //    add edge to polygon
                        if (false == IsEdgeInTriangles(edge, badT, badTriangles))
                            polygon.Add(edge);
                    }

                }

                //for each triangle in badTriangles do // remove them from the data structure
                foreach (var badT in badTriangles)
                {
                    //remove triangle from triangulation
                    triangulation.Remove(badT);
                }

                //for each edge in polygon do // re-triangulate the polygonal hole
                foreach (var edge in polygon)
                {
                    //newTri:= form a triangle from edge to point
                    //add newTri to triangulation
                    Triangle newTriangle = new Triangle(vertex, edge.U, edge.V);
                    triangulation.Add(newTriangle);
                }

            }

            triangulation.RemoveAll(t => t.ContainsVertex(p1) || t.ContainsVertex(p2) || t.ContainsVertex(p3));

            this.triangles = triangulation;

            edges.Clear();
            foreach (var t in triangles)
            {
                edges.AddRange(t.GenerateEdges());
            }

        }

        void TriangulateV3()
        {
            float minX = Vertices[0].Position.x;
            float minY = Vertices[0].Position.y;
            float minZ = Vertices[0].Position.z;
            float maxX = minX;
            float maxY = minY;
            float maxZ = minZ;

            foreach (var vertex in Vertices)
            {
                if (vertex.Position.x < minX) minX = vertex.Position.x;
                if (vertex.Position.x > maxX) maxX = vertex.Position.x;
                if (vertex.Position.y < minY) minY = vertex.Position.y;
                if (vertex.Position.y > maxY) maxY = vertex.Position.y;
                if (vertex.Position.z < minZ) minZ = vertex.Position.z;
                if (vertex.Position.z > maxZ) maxZ = vertex.Position.z;
            }

            float dx = maxX - minX;
            float dy = maxY - minY;
            float dz = maxZ - minZ;
            float deltaMax = Mathf.Max(dx, dy, dz) * 1.41421356f ;

            Vertex p1 = new Vertex(new Vector3(minX - 1, minY - 1, minZ - 1));
            Vertex p2 = new Vertex(new Vector3(maxX + deltaMax, minY - 1, minZ - 1));
            Vertex p3 = new Vertex(new Vector3(minX - 1, maxY + deltaMax, minZ - 1));
            Vertex p4 = new Vertex(new Vector3(minX - 1, minY - 1, maxZ + deltaMax));

            SuperTetrahedron = new Tetrahedron(p1, p2, p3, p4);
            Tetrahedra.Add(SuperTetrahedron);

            //Generate tetrahedros
            foreach (var vertex in Vertices)
            {
                HashSet<Tetrahedron> badTetras = new HashSet<Tetrahedron>();

                // for each triangle in triangulation do // !first find all the triangles that are no longer valid due to the insertion
                foreach (var t in Tetrahedra)
                {
                    //if point is inside circumcircle of triangle
                    if (t.CircumCircleContains(vertex.Position))
                    {
                        badTetras.Add(t);
                    }
                }

                //polygon := empty set
                var polygon = new List<Triangle>();

                //for each triangle in badTriangles do // find the boundary of the polygonal hole
                foreach (var badT in badTetras)
                {
                    //for each edge in triangle do
                    var badTriangles = badT.GenerateTriangles();
                    foreach (var tri in badTriangles)
                    {
                        //  if edge is not shared by any other triangles in badTriangles
                        //    add edge to polygon
                        if (false == IsTriangleInTetrahedro(tri, badT, badTetras))
                            polygon.Add(tri);
                    }

                }

                //for each triangle in badTriangles do // remove them from the data structure
                foreach (var badT in badTetras)
                {
                    //remove triangle from triangulation
                    Tetrahedra.Remove(badT);
                }

                foreach (var triangle in polygon)
                    Tetrahedra.Add(new Tetrahedron(triangle.a, triangle.b, triangle.c, vertex));

#if false
                List<Triangle> triangles = new List<Triangle>();

                foreach (var t in Tetrahedra)
                {
                    if (t.CircumCircleContains(vertex.Position))
                    {
                        t.IsBad = true;
                        triangles.Add(new Triangle(t.A, t.B, t.C));
                        triangles.Add(new Triangle(t.A, t.B, t.D));
                        triangles.Add(new Triangle(t.A, t.C, t.D));
                        triangles.Add(new Triangle(t.B, t.C, t.D));
                    }
                }

                for (int i = 0; i < triangles.Count; i++)
                {
                    for (int j = i + 1; j < triangles.Count; j++)
                    {
                        if (Triangle.AlmostEqual(triangles[i], triangles[j]))
                        {
                            triangles[i].IsBad = true;
                            triangles[j].IsBad = true;
                        }
                    }
                }

                Tetrahedra.RemoveAll((Tetrahedron t) => t.IsBad);
                triangles.RemoveAll((Triangle t) => t.IsBad);

                foreach (var triangle in triangles)
                {
                    Tetrahedra.Add(new Tetrahedron(triangle.a, triangle.b, triangle.c, vertex));
                }
#endif
            }

            //Tetrahedra.Remove(SuperTetrahedron);
            //Tetrahedra.RemoveAll((Tetrahedron t) => t.ContainsVertex(p1) || t.ContainsVertex(p2) || t.ContainsVertex(p3) || t.ContainsVertex(p4));

            HashSet<Triangle> triangleSet = new HashSet<Triangle>();
            HashSet<Edge> edgeSet = new HashSet<Edge>();

            foreach (var t in Tetrahedra)
            {

                var abc = new Triangle(t.A, t.B, t.C);
                var abd = new Triangle(t.A, t.B, t.D);
                var acd = new Triangle(t.A, t.C, t.D);
                var bcd = new Triangle(t.B, t.C, t.D);

                Triangle[] triangles = new Triangle[] { abc, abd, acd, bcd };
                for (int iTris = 0; iTris < triangles.Length; iTris++)
                {
                    if (triangleSet.Add(triangles[iTris]))
                        Triangles.Add(triangles[iTris]);
                }

                var ab = new Edge(t.A, t.B);
                var bc = new Edge(t.B, t.C);
                var ca = new Edge(t.C, t.A);
                var da = new Edge(t.D, t.A);
                var db = new Edge(t.D, t.B);
                var dc = new Edge(t.D, t.C);

                Edge[] edges = new Edge[] { ab, bc, ca, da, db, dc };
                for (int iEdge = 0; iEdge < edges.Length; iEdge++)
                {
                    if (edgeSet.Add(edges[iEdge]))
                        Edges.Add(edges[iEdge]);
                }

            }

        }

        void TriangulateV4()
        {
            float minX = Vertices[0].Position.x;
            float minY = Vertices[0].Position.y;
            float minZ = Vertices[0].Position.z;
            float maxX = minX;
            float maxY = minY;
            float maxZ = minZ;

            foreach (var vertex in Vertices)
            {
                if (vertex.Position.x < minX) minX = vertex.Position.x;
                if (vertex.Position.x > maxX) maxX = vertex.Position.x;
                if (vertex.Position.y < minY) minY = vertex.Position.y;
                if (vertex.Position.y > maxY) maxY = vertex.Position.y;
                if (vertex.Position.z < minZ) minZ = vertex.Position.z;
                if (vertex.Position.z > maxZ) maxZ = vertex.Position.z;
            }

            float dx = maxX - minX;
            float dy = maxY - minY;
            float dz = maxZ - minZ;
            float deltaMax = Mathf.Max(dx, dy, dz) * 1.41421356f;

            Vertex p1 = new Vertex(new Vector3(minX - 1, minY - 1, minZ - 1));
            Vertex p2 = new Vertex(new Vector3(maxX + deltaMax, minY - 1, minZ - 1));
            Vertex p3 = new Vertex(new Vector3(minX - 1, maxY + deltaMax, minZ - 1));
            Vertex p4 = new Vertex(new Vector3(minX - 1, minY - 1, maxZ + deltaMax));

            SuperTetrahedron = new Tetrahedron(p1, p2, p3, p4);
            Tetrahedra.Add(SuperTetrahedron);

            //Generate tetrahedros
            foreach (var vertex in Vertices)
            {
                HashSet<Tetrahedron> badTetras = new HashSet<Tetrahedron>();

                // for each triangle in triangulation do // !first find all the triangles that are no longer valid due to the insertion
                foreach (var t in Tetrahedra)
                {
                    //if point is inside circumcircle of triangle
                    if (t.CircumCircleContains(vertex.Position))
                    {
                        badTetras.Add(t);
                    }
                }

                //polygon := empty set
                var polygon = new List<Triangle>();

                //for each triangle in badTriangles do // find the boundary of the polygonal hole
                foreach (var badT in badTetras)
                {
                    //for each edge in triangle do
                    var badTriangles = badT.GenerateTriangles();
                    foreach (var tri in badTriangles)
                    {
                        //  if edge is not shared by any other triangles in badTriangles
                        //    add edge to polygon
                        if (false == IsTriangleInTetrahedro(tri, badT, badTetras))
                            polygon.Add(tri);
                    }

                }

                //for each triangle in badTriangles do // remove them from the data structure
                foreach (var badT in badTetras)
                {
                    //remove triangle from triangulation
                    Tetrahedra.Remove(badT);
                }

                foreach (var triangle in polygon)
                {
                    var newTetra = new Tetrahedron(triangle.a, triangle.b, triangle.c, vertex);
                    Tetrahedra.Add(newTetra);

                    Triangles.AddRange(newTetra.GenerateTriangles());
                }

            }

            Triangles.RemoveAll(t => t.ContainsVertex(p1) || t.ContainsVertex(p2) || t.ContainsVertex(p3) || t.ContainsVertex(p4));
            Tetrahedra.RemoveAll((Tetrahedron t) => t.ContainsVertex(p1) || t.ContainsVertex(p2) || t.ContainsVertex(p3) || t.ContainsVertex(p4));

            HashSet<Triangle> triangleSet = new HashSet<Triangle>();
            HashSet<Edge> edgeSet = new HashSet<Edge>();

            foreach (var t in Triangles)
            {
                var edges = t.GenerateEdges();

                foreach (var e in edges)
                {
                    if (edgeSet.Add(e))
                        Edges.Add(e);
                }
                
            }


            //foreach (var t in Tetrahedra)
            //{

            //    //var abc = new Triangle(t.A, t.B, t.C);
            //    //var abd = new Triangle(t.A, t.B, t.D);
            //    //var acd = new Triangle(t.A, t.C, t.D);
            //    //var bcd = new Triangle(t.B, t.C, t.D);

            //    //Triangle[] triangles = new Triangle[] { abc, abd, acd, bcd };
            //    //for (int iTris = 0; iTris < triangles.Length; iTris++)
            //    //{
            //    //    if (triangleSet.Add(triangles[iTris]))
            //    //        Triangles.Add(triangles[iTris]);
            //    //}

            //    var ab = new Edge(t.A, t.B);
            //    var bc = new Edge(t.B, t.C);
            //    var ca = new Edge(t.C, t.A);
            //    var da = new Edge(t.D, t.A);
            //    var db = new Edge(t.D, t.B);
            //    var dc = new Edge(t.D, t.C);

            //    Edge[] edges = new Edge[] { ab, bc, ca, da, db, dc };
            //    for (int iEdge = 0; iEdge < edges.Length; iEdge++)
            //    {
            //        if (edgeSet.Add(edges[iEdge]))
            //            Edges.Add(edges[iEdge]);
            //    }

            //}

        }

#endif

        bool IsEdgeInTriangles(Edge edge, Triangle ignoreTriangle, HashSet<Triangle> triangles)
        {
            foreach (var t in triangles)
            {
                if (t == ignoreTriangle) continue;

                if (t.ContainsEdge(edge))
                    return true;
            }

            return false;
        }

        bool IsTriangleInTetrahedro(Triangle tri, Tetrahedron ignoreTetra, HashSet<Tetrahedron> tetras)
        {
            foreach (var t in tetras)
            {
                if (t == ignoreTetra) continue;

                if (t.ContainsTriangle(tri))
                    return true;
            }

            return false;
        }

        public List<Vertex> Vertices => vertices;
        public List<Edge> Edges => edges;
        public List<Triangle> Triangles => triangles;
        public List<Tetrahedron> Tetrahedra => tetrahedra;

    }

}
