using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Generation
{
    public static class MathExtensions 
    {
        public static Vector3 ToVector3(this Vector3Int v)
        {
            return new Vector3(v.x, v.y, v.z);
        }
    }

}