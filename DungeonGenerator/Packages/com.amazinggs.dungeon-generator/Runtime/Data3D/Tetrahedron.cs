using System.Collections.Generic;
using UnityEngine;
using Graphs;
using System;

namespace Delaunay
{
    [System.Serializable]
    public class Tetrahedron : IEquatable<Tetrahedron>
    {
        public Vertex A;
        public Vertex B;
        public Vertex C;
        public Vertex D;

        public bool IsBad { get; set; }

        Vector3 Circumcenter { get; set; }
        float CircumradiusSquared { get; set; }

        public Tetrahedron(Vertex a, Vertex b, Vertex c, Vertex d)
        {
            A = a;
            B = b;
            C = c;
            D = d;
            CalculateCircumsphere();
        }

        void CalculateCircumsphere()
        {
            //calculate the circumsphere of a tetrahedron
            //http://mathworld.wolfram.com/Circumsphere.html

            float a = new Matrix4x4(
                new Vector4(A.Position.x, B.Position.x, C.Position.x, D.Position.x),
                new Vector4(A.Position.y, B.Position.y, C.Position.y, D.Position.y),
                new Vector4(A.Position.z, B.Position.z, C.Position.z, D.Position.z),
                new Vector4(1, 1, 1, 1)
            ).determinant;

            float aPosSqr = A.Position.sqrMagnitude;
            float bPosSqr = B.Position.sqrMagnitude;
            float cPosSqr = C.Position.sqrMagnitude;
            float dPosSqr = D.Position.sqrMagnitude;

            float Dx = new Matrix4x4(
                new Vector4(aPosSqr, bPosSqr, cPosSqr, dPosSqr),
                new Vector4(A.Position.y, B.Position.y, C.Position.y, D.Position.y),
                new Vector4(A.Position.z, B.Position.z, C.Position.z, D.Position.z),
                new Vector4(1, 1, 1, 1)
            ).determinant;

            float Dy = -(new Matrix4x4(
                new Vector4(aPosSqr, bPosSqr, cPosSqr, dPosSqr),
                new Vector4(A.Position.x, B.Position.x, C.Position.x, D.Position.x),
                new Vector4(A.Position.z, B.Position.z, C.Position.z, D.Position.z),
                new Vector4(1, 1, 1, 1)
            ).determinant);

            float Dz = new Matrix4x4(
                new Vector4(aPosSqr, bPosSqr, cPosSqr, dPosSqr),
                new Vector4(A.Position.x, B.Position.x, C.Position.x, D.Position.x),
                new Vector4(A.Position.y, B.Position.y, C.Position.y, D.Position.y),
                new Vector4(1, 1, 1, 1)
            ).determinant;

            float c = new Matrix4x4(
                new Vector4(aPosSqr, bPosSqr, cPosSqr, dPosSqr),
                new Vector4(A.Position.x, B.Position.x, C.Position.x, D.Position.x),
                new Vector4(A.Position.y, B.Position.y, C.Position.y, D.Position.y),
                new Vector4(A.Position.z, B.Position.z, C.Position.z, D.Position.z)
            ).determinant;

            Circumcenter = new Vector3(
                Dx / (2 * a),
                Dy / (2 * a),
                Dz / (2 * a)
            );

            CircumradiusSquared = ((Dx * Dx) + (Dy * Dy) + (Dz * Dz) - (4 * a * c)) / (4 * a * a);
        }

        public bool ContainsVertex(Vertex v)
        {
            return Delaunay3D.AlmostEqual(v, A)
                || Delaunay3D.AlmostEqual(v, B)
                || Delaunay3D.AlmostEqual(v, C)
                || Delaunay3D.AlmostEqual(v, D);
        }

        public bool CircumCircleContains(Vector3 v)
        {
            Vector3 dist = v - Circumcenter;
            return dist.sqrMagnitude <= CircumradiusSquared;
        }

        public void DrawCircumSphere()
        {
            Gizmos.DrawSphere(Circumcenter, 0.1f);
            Gizmos.DrawWireSphere(this.Circumcenter, Mathf.Sqrt(this.CircumradiusSquared));
        }

        public static bool operator ==(Tetrahedron left, Tetrahedron right)
        {
            return (left.A == right.A || left.A == right.B || left.A == right.C || left.A == right.D)
                && (left.B == right.A || left.B == right.B || left.B == right.C || left.B == right.D)
                && (left.C == right.A || left.C == right.B || left.C == right.C || left.C == right.D)
                && (left.D == right.A || left.D == right.B || left.D == right.C || left.D == right.D);
        }

        public static bool operator !=(Tetrahedron left, Tetrahedron right)
        {
            return !(left == right);
        }

        public Triangle[] GenerateTriangles()
        {
            var abc = new Triangle(A, B, C);
            var abd = new Triangle(A, B, D);
            var acd = new Triangle(A, C, D);
            var bcd = new Triangle(B, C, D);

            return new Triangle[] { abc, abd, acd, bcd };
        }

        public override bool Equals(object obj)
        {
            if (obj is Tetrahedron t)
            {
                return this == t;
            }

            return false;
        }

        public bool Equals(Tetrahedron t)
        {
            return this == t;
        }

        public override int GetHashCode()
        {
            return A.GetHashCode() ^ B.GetHashCode() ^ C.GetHashCode() ^ D.GetHashCode();
        }

        public void DrawTetrahedron(bool drawCircumSphere = true)
        {
            Gizmos.DrawLine(A.Position, B.Position);
            Gizmos.DrawLine(A.Position, C.Position);
            Gizmos.DrawLine(A.Position, D.Position);

            Gizmos.DrawLine(B.Position, C.Position);
            Gizmos.DrawLine(B.Position, D.Position);
            Gizmos.DrawLine(C.Position, D.Position);

            if (drawCircumSphere)
                DrawCircumSphere();
        }

        public bool ContainsTriangle(Triangle testTriangle)
        {
            var myTriangles = GenerateTriangles();

            for (int iEdge = 0; iEdge < myTriangles.Length; iEdge++)
            {
                bool same = myTriangles[iEdge] == testTriangle;
                if (same)
                    return true;
            }

            return false;
        }
    }

}