using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Graphs;

namespace Delaunay
{

    [System.Serializable]
    public class Triangle
    {
        public Vertex a;
        public Vertex b;
        public Vertex c;

        Vector3 CircumCenter { get; set; }
        float CircumRadius { get; set; }

        public bool IsBad { get; set; }

        public Triangle(Vertex u, Vertex v, Vertex w)
        {
            a = u;
            b = v;
            c = w;

            this.CalculateCircumsphere();
        }

        public static bool operator ==(Triangle left, Triangle right)
        {
            return (left.a == right.a || left.a == right.b || left.a == right.c)
                && (left.b == right.a || left.b == right.b || left.b == right.c)
                && (left.c == right.a || left.c == right.b || left.c == right.c);
        }

        public static bool operator !=(Triangle left, Triangle right)
        {
            return !(left == right);
        }

        public override bool Equals(object obj)
        {
            if (obj is Triangle e)
            {
                return this == e;
            }

            return false;
        }

        public bool Equals(Triangle e)
        {
            return this == e;
        }

        public override int GetHashCode()
        {
            return a.GetHashCode() ^ b.GetHashCode() ^ c.GetHashCode();
        }

        public static bool AlmostEqual(Triangle left, Triangle right)
        {
            return (Delaunay3D.AlmostEqual(left.a, right.a) || Delaunay3D.AlmostEqual(left.a, right.b) || Delaunay3D.AlmostEqual(left.a, right.c))
                && (Delaunay3D.AlmostEqual(left.b, right.a) || Delaunay3D.AlmostEqual(left.b, right.b) || Delaunay3D.AlmostEqual(left.b, right.c))
                && (Delaunay3D.AlmostEqual(left.c, right.a) || Delaunay3D.AlmostEqual(left.c, right.b) || Delaunay3D.AlmostEqual(left.c, right.c));
        }

        void CalculateCircumsphere()
        {
            Vector3 a = this.a.Position;
            Vector3 b = this.b.Position;
            Vector3 c = this.c.Position;


            float circumX = a.x + b.x + c.x / 3f;
            float circumY = a.y + b.y + c.y / 3f;
            float circumZ = a.z + b.z + c.z / 3f;

            //float ab = a.sqrMagnitude;
            //float cd = b.sqrMagnitude;
            //float ef = c.sqrMagnitude;
            //float circumX = (ab * (c.y - b.y) + cd * (a.y - c.y) + ef * (b.y - a.y)) / (a.x * (c.y - b.y) + b.x * (a.y - c.y) + c.x * (b.y - a.y));
            //float circumY = (ab * (c.x - b.x) + cd * (a.x - c.x) + ef * (b.x - a.x)) / (a.y * (c.x - b.x) + b.y * (a.x - c.x) + c.y * (b.x - a.x));
            //float circumZ = (ab * (c.z - b.z) + cd * (a.z - c.z) + ef * (b.z - a.x)) / (a.y * (c.x - b.x) + b.y * (a.x - c.x) + c.y * (b.x - a.x));

            CircumCenter = new Vector3(circumX , circumY , circumZ) * 0.5f;
            CircumRadius = Vector3.SqrMagnitude(a - CircumCenter);
        }

        public bool CircumCircleContains(Vector3 v)
        {
            Vector3 dist = v - CircumCenter;
            return dist.magnitude <= CircumRadius;
        }

        public Edge[] GenerateEdges()
        {
            var ab = new Edge(a, b);
            var bc = new Edge(b, c);
            var ca = new Edge(c, a);

            return new Edge[] { ab, bc, ca};
        }

        public bool ContainsEdge(Edge testEdge)
        {
            var edges = GenerateEdges();

            for (int iEdge = 0; iEdge < edges.Length; iEdge++)
            {
                bool same = edges[iEdge].U == testEdge.U && edges[iEdge].V == testEdge.V;
                if (same)
                    return true;

                bool sameReverse = edges[iEdge].V == testEdge.U && edges[iEdge].U == testEdge.V;
                if (sameReverse)
                    return true;
            }

            return false;
        }

        public bool ContainsVertex(Vertex v)
        {
            return Delaunay3D.AlmostEqual(v, a)
                || Delaunay3D.AlmostEqual(v, b)
                || Delaunay3D.AlmostEqual(v, c);
        }

        public void DrawTriangle(bool drawCircumSphere = true)
        {
            Gizmos.DrawLine(a.Position, b.Position);
            Gizmos.DrawLine(b.Position, c.Position);
            Gizmos.DrawLine(c.Position, a.Position);

            if(drawCircumSphere)
                this.DrawCircumSphere();
        }

        public void DrawCircumSphere()
        {
            Gizmos.DrawSphere(CircumCenter, 0.1f);
            Gizmos.DrawWireSphere(CircumCenter, Mathf.Sqrt(this.CircumRadius));
        }
    }
}