using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Generation.Interior
{
    [System.Serializable]
    public class DoorRoomData 
    {
        public Vector3Int roomCoordinate;
        public WallOrientation doorOrientations;

        public bool HasAny => doorOrientations != 0;

        public bool HasDoor(WallOrientation orientation)
        {
            return (doorOrientations & orientation) != 0;
        }
    }

}