using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Generation.Interior
{
    [System.Serializable]
    public class FloorData
    {
        int m_FloorHeight;
        [SerializeField] List<RoomData> m_RoomDatas;

        public FloorData(int floorY, BuildingExteriorData exteriorData)
        {
            m_FloorHeight = floorY;
            m_RoomDatas = new();

            var horizontalSize = exteriorData.Bounds.size;
            for (int x = 0; x < horizontalSize.x; x++)
            {
                for (int z = 0; z < horizontalSize.z; z++)
                {
                    var tileCoord = new Vector3Int(x, floorY, z);

                    var doorData = exteriorData.GetDoorRoomData(tileCoord);
                    var roomData = new RoomData(tileCoord, exteriorData.TileScale, doorData);

                    m_RoomDatas.Add(roomData);
                }
            }

        }

        public RoomData GetRoom(int x, int z)
        {
            return m_RoomDatas.First(r => r.roomPosition.x == x && r.roomPosition.z == z);
        }

        public DoorRoomData[] GetDoorRooms()
        {
            var doorRooms = new List<DoorRoomData>();

            foreach (var room in m_RoomDatas)
            {
                if (room.HasDoors)
                    doorRooms.Add(room.Doors);
            }

            if(doorRooms.Count == 0)
                return null;

            return doorRooms.ToArray();
        }

        public void ShowDebugFloor(float tileScale)
        {
            foreach (var room in m_RoomDatas)
            {
                room.ShowDebugRoom(tileScale,true);
            }
        }

        public int FloorHeight => m_FloorHeight;
        public RoomData[] Rooms => m_RoomDatas.ToArray();

        internal static bool DoesNeitherRoomHaveDoors(int x, int z, FloorData bottomFloor, FloorData topFloor)
        {
            var botRoom = bottomFloor.GetRoom(x, z);
            var topRoom = topFloor.GetRoom(x, z);

            if (DoesNeitherRoomHaveDoors(botRoom, topRoom))
                return true;

            return false;
        }

        //both rooms have doors
        internal static bool DoesNeitherRoomHaveDoors(RoomData botRoom, RoomData topRoom)
        {
            if (botRoom == topRoom)
                return false;

            if (botRoom.roomPosition.x == topRoom.roomPosition.x &&
               botRoom.roomPosition.z == topRoom.roomPosition.z)
            {
                return !botRoom.HasDoors && !topRoom.HasDoors;
            }

            Debug.LogError("DoesEitherRoomHaveDoors >> rooms have different horizontal position");
            return false;
        }
    }
}