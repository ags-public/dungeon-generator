using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;
using Graphs;
using Delaunay;

namespace Generation
{
    [System.Serializable]
    public class DungeonSettings
    {
        public readonly static Vector3 k_HorizontalCenterOffset = new Vector3(0.5f, 0f, 0.5f);

        public int RoomAmount = 8;
        public Vector3Int GridDimensions = new Vector3Int(16, 1, 16);
        public float BuildingTileScale = 8f;// m x m size of tile
    }

    public abstract class DungeonGeneratorBase : MonoBehaviour
    {
        protected enum TileType
        {
            None,
            Room,
            Hall,
            Stairs
        }

        [SerializeField] protected int m_Seed = 0;
        [SerializeField] protected DungeonSettings m_DungeonSettings;

        protected List<BuildingExteriorData> m_BuildingsData;
        protected List<Vector3Int[]> m_Paths;

        Grid3D<TileType> m_Grid;
        HashSet<Prim.Edge> m_SelectedEdges;

        Random m_Random;

        [SerializeField] Delaunay3D m_Delaunay;

        public System.Action OnDungeonCreated;

        [Header ("Debug")]
        public bool debugGrid;
        public bool debugRooms;

        public bool debugDelaunay;
        public bool debugSelectedEdges;
        public bool debugTriangleSphere;

        public List<Vector3Int> m_DebugDoorInsideTiles;


        public virtual void CreateDungeon(int seed = -1)
        {
            if (seed >= 0)
                m_Seed = seed;

            m_Random = new Random(m_Seed);
            m_Grid = new Grid3D<TileType>(m_DungeonSettings.GridDimensions, Vector3Int.zero);
            m_BuildingsData = new List<BuildingExteriorData>();

            m_Paths = new();
            m_DebugDoorInsideTiles = new();

            PlaceRooms();
            Triangulate();
            CreateHallways();
            PathfindHallways();

            SpawnBuildings();
            SpawnHallways();

            OnDungeonCreated?.Invoke();
        }

        public virtual void DestroyDungeon() { }

        protected abstract void PlaceRooms();

        void SpawnBuildings()
        {
            foreach (var room in m_BuildingsData)
            {
                SpawnBuilding(room);
            }
        }

        void SpawnHallways()
        {
            foreach (var path in m_Paths)
            {
                foreach (var pos in path)
                {
                    if (m_Grid[pos] == TileType.Hall && !m_Grid.HasSpawned(pos))
                        SpawnHallway(pos);
                }
            }
        }

        void Triangulate()
        {
            if (m_BuildingsData.Count > 0)
            {
                List<Vertex> vertices = new List<Vertex>();

                foreach (var room in m_BuildingsData)
                {
                    var vertex = new Vertex<BuildingExteriorData>((Vector3)room.Bounds.position + ((Vector3)room.Bounds.size) * 0.5f, room);
                    vertices.Add(vertex);
                }

                m_Delaunay = Delaunay3D.Triangulate(vertices);
            }
            else
            {
                Debug.LogError("No rooms added");
            }
        }

        void CreateHallways()
        {
            List<Prim.Edge> edges = new List<Prim.Edge>();

            foreach (var edge in m_Delaunay.Edges)
            {
                edges.Add(new Prim.Edge(edge.U, edge.V));
            }

            List<Prim.Edge> minimumSpanningTree = Prim.MinimumSpanningTree(edges, edges[0].U);

            m_SelectedEdges = new HashSet<Prim.Edge>(minimumSpanningTree);
            var remainingEdges = new HashSet<Prim.Edge>(edges);
            remainingEdges.ExceptWith(m_SelectedEdges);

            foreach (var edge in remainingEdges)
            {
                if (m_Random.NextDouble() < 0.125)
                {
                    m_SelectedEdges.Add(edge);
                }
            }
        }

        void PathfindHallways()
        {
            DungeonPathfinder3D aStar = new DungeonPathfinder3D(m_DungeonSettings.GridDimensions);

            foreach (var edge in m_SelectedEdges)
            {
                var startRoom = (edge.U as Vertex<BuildingExteriorData>).Item;
                var endRoom = (edge.V as Vertex<BuildingExteriorData>).Item;

                var startPosf = startRoom.Bounds.center;
                startPosf.y = m_Random.Next(startRoom.Bounds.yMin, startRoom.Bounds.yMax);
                var endPosf = endRoom.Bounds.center;
                endPosf.y = m_Random.Next(endRoom.Bounds.yMin, endRoom.Bounds.yMax);

                var startPos = new Vector3Int((int)startPosf.x, (int)startPosf.y, (int)startPosf.z);
                var endPos = new Vector3Int((int)endPosf.x, (int)endPosf.y, (int)endPosf.z);

                var path = FindPath(aStar, startPos, endPos);

                if (path != null)
                {
                    path.RemoveAll(tile => startRoom.Bounds.Contains(tile));
                    path.RemoveAll(tile => endRoom.Bounds.Contains(tile));

                    var startDoor = path[0];
                    AddDoors(startRoom, startDoor);

                    var endDoor = path[path.Count - 1];
                    AddDoors(endRoom, endDoor);

                    for (int i = 0; i < path.Count; i++)
                    {
                        var current = path[i];

                        if (m_Grid[current] == TileType.None)
                            m_Grid[current] = TileType.Hall;

                        if (i > 0)
                        {
                            var prev = path[i - 1];
                            var delta = current - prev;

                            if (delta.y != 0)
                                PlaceStair(prev, delta);
                        }
                    }

                    m_Paths.Add(path.ToArray());
                }
            }
        }

        protected void PlaceBuilding(BuildingExteriorData data)
        {
            foreach (var pos in data.Bounds.allPositionsWithin)
            {
                m_Grid[pos] = TileType.Room;
            }

            //mark roof tiles
            int y = data.Bounds.yMax;
            for (int x = data.Bounds.xMin; x < data.Bounds.xMax; x++)
            {
                for (int z = data.Bounds.zMin; z < data.Bounds.zMax; z++)
                {
                    m_Grid[new Vector3Int(x,y,z)] = TileType.Room;
                }
            }
        }

        void AddDoors(BuildingExteriorData room, Vector3Int doorPosition)
        {
            var closestRoomTile = room.FindClosestTile(doorPosition);
            Vector3 direction = (doorPosition) - closestRoomTile;
            
            //m_DebugDoorOutsideTiles.Add(doorPosition);
            m_DebugDoorInsideTiles.Add(closestRoomTile);

            if (direction.x > 0.5)
            {
                room.AddDoor(closestRoomTile, WallOrientation.PositiveX);
                return;
            }
            if (direction.x < -0.5)
            {
                room.AddDoor(closestRoomTile, WallOrientation.NegativeX);
                return;
            }

            if (direction.z > 0.5)
            {
                room.AddDoor(closestRoomTile, WallOrientation.PositiveZ);
                return;
            }
            if (direction.z < -0.5)
            {
                room.AddDoor(closestRoomTile, WallOrientation.NegativeZ);
                return;
            }
        }

        List<Vector3Int> FindPath(DungeonPathfinder3D aStar, Vector3Int startPos, Vector3Int endPos)
        {
            return aStar.FindPath(startPos, endPos, (DungeonPathfinder3D.Node a, DungeonPathfinder3D.Node b) =>
            {
                var pathCost = new DungeonPathfinder3D.PathCost();

                var delta = b.Position - a.Position;

                if (delta.y == 0)
                {
                    //flat hallway
                    pathCost.cost = Vector3Int.Distance(b.Position, endPos);    //heuristic

                    if (m_Grid[b.Position] == TileType.Stairs)
                    {
                        return pathCost;
                    }
                    else if (m_Grid[b.Position] == TileType.Room)
                    {
                        pathCost.cost += 5;
                    }
                    else if (m_Grid[b.Position] == TileType.None)
                    {
                        pathCost.cost += 1;
                    }

                    pathCost.traversable = true;
                }
                else
                {
                    //staircase
                    if ((m_Grid[a.Position] != TileType.None && m_Grid[a.Position] != TileType.Hall)
                        || (m_Grid[b.Position] != TileType.None && m_Grid[b.Position] != TileType.Hall)) return pathCost;

                    pathCost.cost = 100 + Vector3Int.Distance(b.Position, endPos);    //base cost + heuristic

                    int xDir = Mathf.Clamp(delta.x, -1, 1);
                    int zDir = Mathf.Clamp(delta.z, -1, 1);
                    Vector3Int verticalOffset = new Vector3Int(0, delta.y, 0);
                    Vector3Int horizontalOffset = new Vector3Int(xDir, 0, zDir);

                    if (!m_Grid.InBounds(a.Position + verticalOffset)
                        || !m_Grid.InBounds(a.Position + horizontalOffset)
                        || !m_Grid.InBounds(a.Position + verticalOffset + horizontalOffset))
                    {
                        return pathCost;
                    }

                    if (m_Grid[a.Position + horizontalOffset] != TileType.None
                        || m_Grid[a.Position + horizontalOffset * 2] != TileType.None
                        || m_Grid[a.Position + verticalOffset + horizontalOffset] != TileType.None
                        || m_Grid[a.Position + verticalOffset + horizontalOffset * 2] != TileType.None)
                    {
                        return pathCost;
                    }

                    pathCost.traversable = true;
                    pathCost.isStairs = true;
                }

                return pathCost;
            });
            
        }

        void PlaceStair(Vector3Int prev, Vector3Int delta)
        {
            int xDir = Mathf.Clamp(delta.x, -1, 1);
            int zDir = Mathf.Clamp(delta.z, -1, 1);
            Vector3Int verticalOffset = new Vector3Int(0, delta.y, 0);
            Vector3Int horizontalOffset = new Vector3Int(xDir, 0, zDir);

            m_Grid[prev + horizontalOffset] = TileType.Stairs;
            m_Grid[prev + horizontalOffset * 2] = TileType.Stairs;
            m_Grid[prev + verticalOffset + horizontalOffset] = TileType.Stairs;
            m_Grid[prev + verticalOffset + horizontalOffset * 2] = TileType.Stairs;

            bool downStairs = delta.y < 0f;

            float yRotation = 0;
            Vector3Int stairOriginPosition = prev + horizontalOffset;

            if (downStairs)
            {
                if (xDir > 0f)
                {
                    yRotation = 90f;
                    stairOriginPosition.z += 1;
                }

                if (zDir < 0)
                {
                    yRotation = 180f;
                    stairOriginPosition.x += 1;
                    stairOriginPosition.z += 1;
                }

                if (xDir < 0)
                {
                    yRotation = 270f;
                    stairOriginPosition.x += 1;
                }
            }
            else
            {
                if (xDir > 0f)
                {
                    yRotation = 90f;
                    stairOriginPosition.z += 1;
                }

                if (zDir < 0)
                {
                    yRotation = 180f;
                    stairOriginPosition.x += 1;
                    stairOriginPosition.z += 1;
                }

                if (xDir < 0)
                {
                    yRotation = 270f;
                    stairOriginPosition.x += 1;
                }
            }

            var rotation = Quaternion.Euler(0, yRotation, 0);
            SpawnStairs(stairOriginPosition, rotation, downStairs);

            m_Grid.MarkSpawned(prev + horizontalOffset);
            m_Grid.MarkSpawned(prev + horizontalOffset * 2);
            m_Grid.MarkSpawned(prev + verticalOffset + horizontalOffset);
            m_Grid.MarkSpawned(prev + verticalOffset + horizontalOffset * 2);
        }

        protected virtual void SpawnBuilding(BuildingExteriorData data)
        {
            m_Grid.MarkSpawned(data.Bounds.position);
        }

        protected virtual void SpawnHallway(Vector3Int location)
        {
            m_Grid.MarkSpawned(location);
        }

        protected abstract void SpawnStairs(Vector3Int location, Quaternion rotation, bool down);

        void OnDrawGizmos()
        {
            DebugGrid();

            DebugRoomOutlines();

            DebugSelectedEdges();

            DebugDelaunay();

            DebugPathLines();

            DebugDoorTiles();
        }

        void DebugSelectedEdges()
        {
            if(Application.isPlaying && debugSelectedEdges)
            {
                Gizmos.color = Color.cyan;
                foreach (var edge in m_SelectedEdges)
                {
                    Gizmos.DrawLine(edge.U.Position * Settings.BuildingTileScale, edge.V.Position * Settings.BuildingTileScale);
                }
            }
        }

        void DebugRoomOutlines()
        {
            if (!debugRooms) return;

            if (m_BuildingsData == null) return;

            for (int iRoom = 0; iRoom < m_BuildingsData.Count; iRoom++)
            {
                Vector3 halfSize = (Vector3)m_BuildingsData[iRoom].Bounds.size * 0.5f;
                Gizmos.DrawWireCube((m_BuildingsData[iRoom].Bounds.position + halfSize) * Settings.BuildingTileScale, halfSize * 2f * Settings.BuildingTileScale);
            }
        }

        void DebugDelaunay()
        {
            if (!debugDelaunay) return;

            if (m_Delaunay == null) return;

            Gizmos.color = Color.green;
            int triangleCount = m_Delaunay.Triangles.Count;
            for (int i = 0; i < triangleCount; i++)
            {
                var tri = m_Delaunay.Triangles[i];
                tri.DrawTriangle(debugTriangleSphere);
            }

            Gizmos.color = Color.blue;
            for (int i = 0; i < m_Delaunay.Vertices.Count; i++)
            {
                Gizmos.DrawSphere(m_Delaunay.Vertices[i].Position, 0.1f);
            }

            Gizmos.color = Color.white;
        }

        void DebugGrid()
        {
            if (!debugGrid)
                return;

            var horizontalOffset = DungeonSettings.k_HorizontalCenterOffset;

            Gizmos.color = Color.white * 0.5f;

            var size = this.m_DungeonSettings.GridDimensions;
            for (int x = 0; x < size.x; x+=1)
            {
                for (int y = 0; y < size.y; y += 1)
                {
                    for (int z = 0; z < size.z; z += 1)
                    {
                        var position = new Vector3(x,y,z) + horizontalOffset * Settings.BuildingTileScale;
                        Gizmos.DrawWireCube(position, Vector3.one * Settings.BuildingTileScale);
                    }
                }

            }
            Gizmos.color = Color.white;
        }

        void DebugPathLines()
        {
            if(Application.isPlaying)
            {
                Gizmos.color = Color.blue;
                var horizontalOffset = DungeonSettings.k_HorizontalCenterOffset;

                foreach (var path in m_Paths)
                {
                    for (int i = 1; i < path.Length; i++)
                    {
                        var prev = path[i - 1];
                        var current = path[i];

                        Vector3 lineStart = (prev.ToVector3() + horizontalOffset) * m_DungeonSettings.BuildingTileScale;
                        Vector3 lineEnd = (current.ToVector3() + horizontalOffset) * m_DungeonSettings.BuildingTileScale;
                        Gizmos.DrawLine(lineStart, lineEnd);
                    }
                }
                Gizmos.color = Color.white;
            }
        }

        void DebugDoorTiles()
        {
            if (Application.isPlaying)
            {
                var horizontalOffset = DungeonSettings.k_HorizontalCenterOffset;

                Gizmos.color = new Color(1f, 0.65f, 0f);
                for (int i = 0; i < m_DebugDoorInsideTiles.Count; i++)
                {
                    Vector3 position = (m_DebugDoorInsideTiles[i].ToVector3() + horizontalOffset) * Settings.BuildingTileScale;
                    Gizmos.DrawCube(position, Vector3.one * Settings.BuildingTileScale * 0.25f);
                }

                Gizmos.color = Color.white;
            }
        }

        public DungeonSettings Settings { get { return m_DungeonSettings; } }

        protected Random RandomSeed { get { return m_Random; } }
    }

}