using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Generation
{
    public class DungeonGeneratorMinimal : DungeonGeneratorBase
    {
        [Header("Minimal")]

        List<BuildingGenerator> m_Buildings;

        [SerializeField] Vector3Int roomSizeMinimum = new Vector3Int(1, 1, 1);
        [SerializeField] Vector3Int roomSizeMaximum = new Vector3Int(2, 2, 2);

        [SerializeField] GameObject roomPrefab;
        [SerializeField] GameObject hallwayPrefab;
        [SerializeField] GameObject stairPrefab;
        [SerializeField] GameObject stairdownPrefab;

        public override void CreateDungeon(int seed = -1)
        {
            m_Buildings = new List<BuildingGenerator>();

            base.CreateDungeon(seed);
        }

        public override void DestroyDungeon()
        {
            for (int iChild = 0; iChild < transform.childCount; iChild++)
            {
               Destroy( transform.GetChild(iChild).gameObject);
            }
        }

        protected override void PlaceRooms()
        {
            Vector3Int mapSize = Settings.GridDimensions;

            for (int i = 0; i < m_DungeonSettings.RoomAmount; i++)
            {
                Vector3Int roomSize = new Vector3Int(
                    RandomSeed.Next(roomSizeMinimum.x, roomSizeMaximum.x + 1),
                    RandomSeed.Next(roomSizeMinimum.y, roomSizeMaximum.y + 1),
                    RandomSeed.Next(roomSizeMinimum.z, roomSizeMaximum.z + 1)
                );

                Vector3Int location = new Vector3Int(
                    RandomSeed.Next(0, mapSize.x - roomSize.x),
                    RandomSeed.Next(0, mapSize.y - roomSize.y),
                    RandomSeed.Next(0, mapSize.z - roomSize.z)
                );


                bool add = true;
                var newRoom = new BuildingExteriorData(i, location, roomSize, Settings.BuildingTileScale);
                var buffer = new BoundsInt(location + new Vector3Int(-1, 0, -1), roomSize + new Vector3Int(2, 0, 2));

                foreach (var room in base.m_BuildingsData)
                {
                    if (room.Intersect( buffer) )
                    {
                        add = false;
                        Debug.LogWarning($"Room.Intersect: {room.Bounds} | buffer {buffer}");
                        break;
                    }
                }

                if (   newRoom.Bounds.xMin < 0 || newRoom.Bounds.xMax > mapSize.x
                    || newRoom.Bounds.yMin < 0 || newRoom.Bounds.yMax > mapSize.y
                    || newRoom.Bounds.zMin < 0 || newRoom.Bounds.zMax > mapSize.z)
                {
                    add = false;
                    Debug.LogWarning($"out of bounds: {newRoom.Bounds}");
                }

                if (add)
                {
                    this.PlaceBuilding(newRoom);
                    base.m_BuildingsData.Add(newRoom);
                    //Debug.Log($"Add room: {newRoom.bounds.ToString()}");
                }
            }

            Debug.Log($"Total rooms spawned: {base.m_BuildingsData.Count}");
        }

        GameObject PlaceCube(GameObject prefab, Vector3Int location, Vector3 size, TileType cellType)
        {
            Vector3 spawnPosition = location.ToVector3() * m_DungeonSettings.BuildingTileScale;
            GameObject go = Instantiate(prefab, spawnPosition, Quaternion.identity);
            go.transform.localScale = size * m_DungeonSettings.BuildingTileScale;

            go.name = $"{cellType}:{location}";
            go.transform.SetParent(transform);
            return go;
        }

        protected override void SpawnBuilding(BuildingExteriorData data)
        {
            base.SpawnBuilding(data);

            var go = PlaceCube(roomPrefab.gameObject, data.Bounds.position, data.Bounds.size, TileType.Room);

            var building = go.GetComponent<BuildingGenerator>();
            building.Init(data);
            m_Buildings.Add(building);
        }

        protected override void SpawnHallway(Vector3Int location)
        {
            base.SpawnHallway(location);

            PlaceCube(hallwayPrefab,location, Vector3.one, TileType.Hall);
        }

        protected override void SpawnStairs(Vector3Int location, Quaternion rotation, bool down)
        {
            var go = PlaceCube(down ? stairdownPrefab : stairPrefab, location, new Vector3Int(1, 1, 1), TileType.Stairs);
            go.transform.rotation = rotation;
        }

        public BuildingGenerator GetRoomBehavior(int index)
        {
            if (index < m_Buildings.Count)
                return m_Buildings[index];
            return null;
        }
    }

}