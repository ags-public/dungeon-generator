using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Generation.Interior
{
    public enum InteriorTileType
    {
        None,
        Door,
        Stair,
        Ladder,
        Entity,
        Blocked
    }

    [System.Serializable]
    public class RoomData
    {
        readonly static Color ColorTileFree  = Color.white;
        readonly static Color ColorTileDoor  = Color.black;
        readonly static Color ColorTileStair = Color.yellow;

        public Vector3Int roomPosition;
        public Grid3D<InteriorTileType> m_Grid;
        DoorRoomData m_RoomDoors;

        public RoomData( Vector3Int coordinate, float buildingTileScale, DoorRoomData roomDoors)
        {
            roomPosition = coordinate;
            m_RoomDoors = roomDoors;

            m_Grid = new Grid3D<InteriorTileType>(new Vector3Int(3, 3, 3), Vector3Int.zero);

            AddDoorData(roomDoors);
            //mark center tiles
            MarkTile(1, 0, 1, InteriorTileType.Blocked);
            MarkTile(1, 1, 1, InteriorTileType.Blocked);
            MarkTile(1, 2, 1, InteriorTileType.Blocked);
        }

        void AddDoorData(DoorRoomData doorRoom)
        {
            if (doorRoom == null) return;

            MarkDoorWall(doorRoom, WallOrientation.PositiveZ);
            MarkDoorWall(doorRoom, WallOrientation.NegativeZ);
            MarkDoorWall(doorRoom, WallOrientation.PositiveX);
            MarkDoorWall(doorRoom, WallOrientation.NegativeX);
        }

        void MarkDoorWall(DoorRoomData doorRoom, WallOrientation orientation)
        {
            if (!doorRoom.HasDoor(orientation)) return;

            //mark tiles
            switch (orientation)
            {
                case WallOrientation.PositiveX:
                    MarkTile(2, 0, 0, InteriorTileType.Door);
                    MarkTile(2, 0, 1, InteriorTileType.Door);
                    MarkTile(2, 0, 2, InteriorTileType.Door);

                    MarkTile(2, 1, 0, InteriorTileType.Door);
                    MarkTile(2, 1, 1, InteriorTileType.Door);
                    MarkTile(2, 1, 2, InteriorTileType.Door);
                    break;
                case WallOrientation.PositiveZ:
                    MarkTile(0, 0, 2, InteriorTileType.Door);
                    MarkTile(1, 0, 2, InteriorTileType.Door);
                    MarkTile(2, 0, 2, InteriorTileType.Door);

                    MarkTile(0, 1, 2, InteriorTileType.Door);
                    MarkTile(1, 1, 2, InteriorTileType.Door);
                    MarkTile(2, 1, 2, InteriorTileType.Door);
                    break;
                case WallOrientation.NegativeX:
                    MarkTile(0, 0, 0, InteriorTileType.Door);
                    MarkTile(0, 0, 1, InteriorTileType.Door);
                    MarkTile(0, 0, 2, InteriorTileType.Door);

                    MarkTile(0, 1, 0, InteriorTileType.Door);
                    MarkTile(0, 1, 1, InteriorTileType.Door);
                    MarkTile(0, 1, 2, InteriorTileType.Door);
                    break;
                case WallOrientation.NegativeZ:
                    MarkTile(0, 0, 0, InteriorTileType.Door);
                    MarkTile(1, 0, 0, InteriorTileType.Door);
                    MarkTile(2, 0, 0, InteriorTileType.Door);

                    MarkTile(0, 1, 0, InteriorTileType.Door);
                    MarkTile(1, 1, 0, InteriorTileType.Door);
                    MarkTile(2, 1, 0, InteriorTileType.Door);
                    break;
            }
        }

        void MarkTile(int x, int y, int z, InteriorTileType interiorTile)
        {
            m_Grid[x, y, z] = interiorTile;
        }

        void MarkTile(Vector3Int coord, InteriorTileType interiorTile)
        {
            m_Grid[coord.x, coord.y, coord.z] = interiorTile;
        }

        public DoorRoomData Doors => m_RoomDoors;

        public void ShowDebugRoom(float buildingScale, bool showGrid)
        {
            if (m_Grid == null) return;

            float roomTileScale = buildingScale * 0.333f;

            for (int x = 0; x < 3; x++)
            {
                for (int y = 0; y < 3; y++)
                {
                    for (int z = 0; z < 3; z++)
                    {
                        var tile = m_Grid[x, y, z];

                        var offset = roomPosition.ToVector3()* buildingScale;
                        var position = (new Vector3(x, y, z) + new Vector3(0.5f,0.5f,0.5f)) * roomTileScale + offset;

                        if(showGrid)
                        {
                            Gizmos.color = Color.cyan * 0.5f;
                            Gizmos.DrawWireCube(position, Vector3.one * roomTileScale);
                        }

                        var tileColor = ColorTileFree;
                        switch (tile)
                        {
                            case InteriorTileType.Door:
                                tileColor = ColorTileDoor;
                                break;

                            case InteriorTileType.Stair:
                            case InteriorTileType.Ladder:
                                tileColor = ColorTileStair;
                                break;

                            case InteriorTileType.Entity:
                                tileColor = Color.green;
                                break;
                            case InteriorTileType.Blocked:
                                tileColor = Color.red;
                                break;
                        }

                        Gizmos.color = tileColor;
                        Gizmos.DrawCube(position, Vector3.one * roomTileScale * 0.25f);
                    }
                }
            }

            Gizmos.color = Color.white;
        }

        public bool HasDoors => m_RoomDoors != null && m_RoomDoors.HasAny;
    }

}