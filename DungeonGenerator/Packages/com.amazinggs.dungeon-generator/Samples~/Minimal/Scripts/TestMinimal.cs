using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Generation;

public class TestMinimal : MonoBehaviour
{
    public DungeonGeneratorMinimal dungeonGenerator;
    public Transform playerTransform;

    void Start()
    {
        dungeonGenerator.OnDungeonCreated += MovePlayer;

        CreateDungeon();
    }

    void CreateDungeon()
    {
        dungeonGenerator.DestroyDungeon();
        dungeonGenerator.CreateDungeon();
    }

    void MovePlayer()
    {
        float tileScale = dungeonGenerator.Settings.TileScale;
        Vector3 offset = DungeonGeneratorBase.k_HorizontalCenterOffset * tileScale + new Vector3(0, 1.5f, 0);
        playerTransform.transform.position = dungeonGenerator.GetRoomBehavior(0).transform.position + offset;
    }
}
