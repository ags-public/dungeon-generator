# Dungeon Generator

## Overview
Unity project that generates 3D dungeons with a click of button. Based on *[Vazgriz work](https://github.com/vazgriz/DungeonGenerator)*, but with additional improvements.
This is a hobby project.

![example](image.png)

## Features
- Deterministic.
- Extendible.

## Installation
No UPM support at the moment. Copy the package folder into your project.

## Usage
Import the 'Minimal' sample project using the Package Manager to get started.

## Roadmap
- Fully robust, no weird edge case failures.
- Generate interiors.

## Support
If you found this useful, feel free to *[buy a me a coffee](https://www.buymeacoffee.com/alexisgamedev)*. 

## Authors and acknowledgment
Thanks to Vazgriz for providing a great starting point.

## License
MIT license.
